import { StatisticalDTO } from './model/statistical';
import { Component, ViewChild } from '@angular/core';
import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexFill,
  ApexStroke,
  ApexYAxis,
  ApexTooltip,
  ApexMarkers,
  ApexXAxis
} from "ng-apexcharts";
import { StatisticalService } from './service/statistical.service';


export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  yaxis: ApexYAxis | ApexYAxis[];
  labels: string[];
  stroke: ApexStroke;
  markers: ApexMarkers;
  fill: ApexFill;
  tooltip: ApexTooltip;
};
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'chartAngular';
  year:number = 2018;
  statisticalDTO: StatisticalDTO[];
  total:any[]=[];
  month:any[]=[];
  @ViewChild("chart") chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;
  constructor(
    public statisticalService: StatisticalService
  
  ) {
    this.getStatistical();

  }
  generateDataChart(){
    this.chartOptions = {
      series: [
        {
          name: "Tổng doanh thu năm "+ this.year,
          type: "area",
          data: this.total
        }
      ],
      chart: {
        height: 350,
        type: "line"
      },
      stroke: {
        curve: "smooth"
      },
      fill: {
        type: "solid",
        opacity: [0.35, 1]
      },
      labels: this.month,
      markers: {
        size: 0
      },
      yaxis: [
        {
          title: {
            text: "Series A"
          }
        },
        {
          opposite: true,
          title: {
            text: "Series B"
          }
        }
      ],
      xaxis: {
        labels: {
          trim: false
        }
      },
      tooltip: {
        shared: true,
        intersect: false,
        y: {
          formatter: function(y) {
            if (typeof y !== "undefined") {
              return y.toFixed(0) + " nghìn đồng";
            }
            return y;
          }
        }
      }
    };
  }
  public getStatistical(){
    console.log(this.year)
    this.statisticalService.getStatistical(this.year).subscribe(
      data => {
       console.log(data)
       this.statisticalDTO = data
       this.total = [];
       this.month = [];
       data.forEach(element => {
         this.total.push(element.total);
         this.month.push("Tháng " + element.month)
       });
     this.generateDataChart()
      }
     
   
      )
  }
 
  public generateData(count, yrange) {
    var i = 0;
    var series = [];
    while (i < count) {
      var x = "w" + (i + 1).toString();
      var y =
        Math.floor(Math.random() * (yrange.max - yrange.min + 1)) + yrange.min;

      series.push({
        x: x,
        y: y
      });
      i++;
    }
    return series;
  } 

}
