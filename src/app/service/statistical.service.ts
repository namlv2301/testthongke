import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
const cabecera = {headers: new HttpHeaders({'Content-Type': 'application/json'})};  
@Injectable( {providedIn: 'root'})
export class StatisticalService {
    api = 'http://localhost:8080/api/user/';
    constructor(private http: HttpClient) { }
    public chart(data: any): Observable<any> {
      return this.http.post<any>(this.api + 'statistical', data);
    }
    public getStatistical(year: any): Observable<any> {
      return this.http.get<any>(this.api + `statistical/${year}`,cabecera);
    }
}
